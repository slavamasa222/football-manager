package com.slavamasa.utils

sealed class State {
    class Success<T>(val data: T): State()
    class Loading: State()
    class Error(val message: String): State()
}