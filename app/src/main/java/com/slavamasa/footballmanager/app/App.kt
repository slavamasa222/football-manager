package com.slavamasa.footballmanager.app

import android.app.Application
import com.onesignal.OneSignal
import com.slavamasa.footballmanager.di.dataModule
import com.slavamasa.footballmanager.di.domainModule
import com.slavamasa.footballmanager.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

private const val ONESIGNAL_APP_ID = "4785be80-0ade-49ad-a90b-96816eb45bcf"

class App : Application() {



    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(dataModule, domainModule, presentationModule)
        }

        // Logging set to help debug issues, remove before releasing your app.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)

        // OneSignal Initialization
        OneSignal.initWithContext(this)
        OneSignal.setAppId(ONESIGNAL_APP_ID)
    }
}