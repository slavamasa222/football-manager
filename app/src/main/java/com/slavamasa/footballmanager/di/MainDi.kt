package com.slavamasa.footballmanager.di

import androidx.room.Room
import androidx.room.RoomDatabase
import com.slavamasa.feature_main.data.data_source.MatchDao
import com.slavamasa.feature_main.data.data_source.MatchDatabase
import com.slavamasa.feature_main.data.remote.socker_api.SportdataApi
import com.slavamasa.feature_main.data.repository.RepositoryImpl
import com.slavamasa.feature_main.domain.repository.Repository
import com.slavamasa.feature_main.domain.usecases.*
import com.slavamasa.feature_main.presentation.adapters.MainRecyclerViewAdapter
import com.slavamasa.feature_main.presentation.viewmodels.SplashViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://app.sportdataapi.com/api/v1/"

val dataModule = module {

    single<MatchDatabase> {
        Room.databaseBuilder(
            androidApplication(),
            MatchDatabase::class.java,
            "MatchDatabase.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    single<MatchDao> { get<MatchDatabase>().matchDao }

    single<SportdataApi> {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(SportdataApi::class.java)
    }

    single<Repository> { RepositoryImpl(sportdataApi = get(), matchDao = get()) }
}

val domainModule = module {

    factory<GetUefaYouthLeagueMatches> { GetUefaYouthLeagueMatches(repository = get()) }

    factory<GetUefaChampionsLeagueMatches> { GetUefaChampionsLeagueMatches(repository = get()) }

    factory<GetAllMatches> { GetAllMatches(repository = get()) }

    factory<InsertMatch> { InsertMatch(repository = get()) }

    factory<SportdataUseCases> {
        SportdataUseCases(
            getUefaYouthLeagueMatches = get(),
            getUefaChampionsLeagueMatches = get(),
            getAllMatches = get(),
            insertMatch = get()
        )
    }
}

val presentationModule = module {

    viewModel<SplashViewModel> {SplashViewModel(sportdataUseCases = get())}
}