package com.slavamasa.feature_main.data.data_source

import androidx.room.Database
import androidx.room.RoomDatabase
import com.slavamasa.feature_main.domain.models.Match

@Database(
    entities = [Match::class],
    version = 1
)
abstract class MatchDatabase : RoomDatabase() {

    abstract val matchDao: MatchDao
}