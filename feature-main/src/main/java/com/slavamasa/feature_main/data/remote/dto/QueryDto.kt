package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName

data class QueryDto(
    @SerializedName("apikey")
    val apikey: String,

    @SerializedName("season_id")
    val seasonId: String
)