package com.slavamasa.feature_main.data.repository

import androidx.lifecycle.LiveData
import com.slavamasa.feature_main.data.data_source.MatchDao
import com.slavamasa.feature_main.data.remote.dto.ResponseDto
import com.slavamasa.feature_main.data.remote.socker_api.SportdataApi
import com.slavamasa.feature_main.domain.models.Match
import com.slavamasa.feature_main.domain.repository.Repository

class RepositoryImpl(
    private val sportdataApi: SportdataApi,
    private val matchDao: MatchDao
) : Repository {

    override suspend fun getUefaYouthLeagueMatches(): ResponseDto {
        return sportdataApi.getUefaYouthLeagueMatches()
    }

    override suspend fun getUefaChampionsLeagueMatches(): ResponseDto {
        return sportdataApi.getUefaChampionsLeagueMatches()
    }

    override suspend fun getAllMatches(): List<Match> {
        return matchDao.getAllMatches()
    }

    override suspend fun insertMatch(match: Match) {
        matchDao.insertMatch(match)
    }


}