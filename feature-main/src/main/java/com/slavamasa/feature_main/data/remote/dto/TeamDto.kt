package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.slavamasa.feature_main.domain.models.Team

data class TeamDto(
    @SerializedName("team_id")
    val teamId: Int,

    @SerializedName("name")
    val name: String?,

    @SerializedName("logo")
    val logo: String?,

    @SerializedName("short_code")
    val shortCode: String?,

    @SerializedName("country")
    val country: CountryDto?
)

fun TeamDto.toTeam(): Team {
    return Team(
        teamId = teamId,
        name = name,
        logo = logo,
        country = country?.toCountry()
    )
}
