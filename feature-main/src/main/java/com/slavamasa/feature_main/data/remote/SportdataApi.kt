package com.slavamasa.feature_main.data.remote.socker_api

import com.slavamasa.feature_main.data.remote.dto.ResponseDto
import retrofit2.http.GET

private const val API_KEY = "e5ac4400-73c9-11ec-83a5-f14e3a0a2502"

interface SportdataApi {

    @GET("soccer/matches?apikey=$API_KEY&season_id=2730")
    suspend fun getUefaYouthLeagueMatches(): ResponseDto

    @GET("soccer/matches?apikey=$API_KEY&season_id=1959")
    suspend fun getUefaChampionsLeagueMatches(): ResponseDto
}