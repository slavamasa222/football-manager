package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName

data class ResponseDto(
    @SerializedName("query")
    val query: QueryDto,

    @SerializedName("data")
    val matches: List<MatchDto>
)