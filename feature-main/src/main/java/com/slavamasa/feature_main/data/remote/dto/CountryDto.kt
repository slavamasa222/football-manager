package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.slavamasa.feature_main.domain.models.Country

data class CountryDto(
    @SerializedName("country_id")
    val countryId: Int,

    @SerializedName("continent")
    val continent: String?,

    @SerializedName("country_code")
    val countryCode: String?,

    @SerializedName("name")
    val name: String?
)

fun CountryDto.toCountry(): Country {
    return Country(countryId = countryId, name = name)
}