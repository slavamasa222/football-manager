package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.slavamasa.feature_main.domain.models.Match

data class MatchDto(
    @SerializedName("match_id")
    val matchId: Int,

    @SerializedName("league_id")
    val leagueId: Int,

    @SerializedName("season_id")
    val seasonId: Int,

    @SerializedName("match_start")
    val matchStart: String?,

    @SerializedName("status")
    val status: String?,

    @SerializedName("status_code")
    val statusCode: Int?,

    @SerializedName("home_team")
    val homeTeam: TeamDto?,

    @SerializedName("away_team")
    val awayTeam: TeamDto?,

    @SerializedName("stats")
    val stats: StatsDto?,

    @SerializedName("venue")
    val venue: VenueDto?
)

fun MatchDto.toMatch(): Match {
    return Match(
        matchId = matchId,
        matchStart = matchStart,
        homeTeam = homeTeam?.toTeam(),
        awayTeam = awayTeam?.toTeam(),
        stats = stats?.toStats()
    )
}