package com.slavamasa.feature_main.data.data_source

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.slavamasa.feature_main.domain.models.Match

@Dao
interface MatchDao {

    @Query("SELECT * FROM `Match`")
    fun getAllMatches(): List<Match>

    @Insert(onConflict = IGNORE)
    fun insertMatch(match: Match)
}