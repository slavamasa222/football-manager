package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.slavamasa.feature_main.domain.models.Stats

data class StatsDto(
    @SerializedName("home_score")
    val homeScore: Int,

    @SerializedName("away_score")
    val awayScore: Int,

    @SerializedName("et_score")
    val etScore: String?,

    @SerializedName("ft_score")
    val ftScore: String,

    @SerializedName("ht_score")
    val htScore: String?,

    @SerializedName("ps_score")
    val psScore: String?
)

fun StatsDto.toStats(): Stats {
    return Stats(
        homeScore = homeScore,
        awayScore = awayScore
    )
}