package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName

data class VenueDto(
    @SerializedName("venue_id")
    val venueId: Int,

    @SerializedName("capacity")
    val capacity: Int?,

    @SerializedName("city")
    val city: String?,

    @SerializedName("country_id")
    val countryId: Int?,

    @SerializedName("name")
    val name: String?
)