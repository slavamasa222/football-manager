package com.slavamasa.feature_main.presentation.screens

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.slavamasa.feature_main.R
import com.slavamasa.feature_main.databinding.FragmentMainBinding
import com.slavamasa.feature_main.domain.models.Match
import com.slavamasa.feature_main.presentation.adapters.MainRecyclerViewAdapter
import com.slavamasa.feature_main.presentation.viewmodels.SplashViewModel
import com.slavamasa.utils.State
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private lateinit var mainRecyclerViewAdapter: MainRecyclerViewAdapter
    private val splashViewModel by sharedViewModel<SplashViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().supportFragmentManager.popBackStack()
            }

        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        initClickListeners()

        splashViewModel.state.observe(viewLifecycleOwner, {
            when (it) {
                is State.Success<*> -> {
                    mainRecyclerViewAdapter.differ.submitList(it.data as List<Match>)
                }
                is State.Loading -> {
                    mainRecyclerViewAdapter.differ.submitList(listOf())
                    Toast.makeText(requireContext(), "Loading...", Toast.LENGTH_SHORT).show()
                }
                is State.Error -> {
                    mainRecyclerViewAdapter.differ.submitList(listOf())
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun initRecyclerView() {

        mainRecyclerViewAdapter = MainRecyclerViewAdapter()
        mainRecyclerViewAdapter.stateRestorationPolicy =
            RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        mainRecyclerViewAdapter.differ.submitList(listOf())
        binding.recyclerViewMain.adapter = mainRecyclerViewAdapter
        binding.recyclerViewMain.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun initClickListeners() {
        binding.btnGoToWebview.setOnClickListener {
            val request = NavDeepLinkRequest.Builder
                .fromUri(requireContext().getString(R.string.nav_path_webview).toUri())
                .build()
            findNavController().navigate(request)
        }
    }

}