package com.slavamasa.feature_main.presentation.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.slavamasa.feature_main.R
import com.slavamasa.feature_main.presentation.viewmodels.SplashViewModel
import com.slavamasa.utils.State
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : Fragment() {

    private val splashViewModel by sharedViewModel<SplashViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()

    }

    private fun initObservers() {
        splashViewModel.state.observe(viewLifecycleOwner, {
            val tv = view?.findViewById<TextView>(R.id.tv)
            when(it) {
                is State.Success<*> -> {
                    findNavController().navigate(R.id.action_splashFragment_to_mainFragment)
                }
                is State.Loading -> {
                    tv?.text = "Подождите, идет загрузка..."
                }
                is State.Error -> {
                    tv?.text = it.message
                }
            }
        })
    }
}