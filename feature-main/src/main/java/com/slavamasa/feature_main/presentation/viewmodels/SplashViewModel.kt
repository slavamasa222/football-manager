package com.slavamasa.feature_main.presentation.viewmodels


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.slavamasa.feature_main.domain.models.Match
import com.slavamasa.feature_main.domain.usecases.SportdataUseCases
import com.slavamasa.utils.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class SplashViewModel(
    private val sportdataUseCases: SportdataUseCases
) : ViewModel() {

    private val _state = MutableLiveData<State>().apply { value = State.Loading() }
    val state: LiveData<State> = _state

    init {
        getUefaYouthLeagueMatches()
    }

    private fun getUefaYouthLeagueMatches() {
        _state.value = State.Loading()
        viewModelScope.launch(Dispatchers.IO) {

            try {
                val uefaYouthLeagueMatches = sportdataUseCases.getUefaYouthLeagueMatches()


                if (uefaYouthLeagueMatches.isEmpty()) {
                    withContext(Dispatchers.Main) {
                        _state.value = State.Error("No matches not found")
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        _state.value = State.Success(uefaYouthLeagueMatches)
                    }
                    fetchDataToDatabase(uefaYouthLeagueMatches)

                }
            } catch (e: HttpException) {
                _state.value = State.Error("Http error")
            } catch (e: IOException) {
                _state.value = State.Error("Unable to access the server")
            }
        }
    }

    private suspend fun fetchDataToDatabase(matches: List<Match>) {
        try {
            matches.forEach { match ->
                sportdataUseCases.insertMatch(match)
            }
        } catch (e: IOException) {
            _state.value = State.Error("Database exception")
        }

    }

}