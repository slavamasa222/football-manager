package com.slavamasa.feature_main.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.slavamasa.feature_main.R
import com.slavamasa.feature_main.domain.models.Match
import java.lang.NullPointerException

class MainRecyclerViewAdapter : RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<Match>() {
        override fun areItemsTheSame(oldItem: Match, newItem: Match): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Match, newItem: Match): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, differCallback)

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val homeTeamName: TextView = view.findViewById(R.id.tv_home_name)
        private val awayTeamName: TextView = view.findViewById(R.id.tv_away_name)
        private val homeTeamScore: TextView = view.findViewById(R.id.tv_home_score)
        private val awayTeamScore: TextView = view.findViewById(R.id.tv_away_score)
        private val homeTeamLogo: ImageView = view.findViewById(R.id.iv_home_logo)
        private val awayTeamLogo: ImageView = view.findViewById(R.id.iv_away_logo)

        fun bind() {
            differ.currentList[adapterPosition].apply {
                homeTeamName.text =
                    homeTeam?.name ?: homeTeamName.context.getString(R.string.no_team_name)
                awayTeamName.text =
                    awayTeam?.name ?: awayTeamName.context.getString(R.string.no_team_name)

                val homeScore = stats?.homeScore ?: 0
                homeTeamScore.text = homeScore.toString()
                val awayScore = stats?.awayScore ?: 0
                awayTeamScore.text = awayScore.toString()

                loadImage(homeTeamLogo, homeTeam?.logo)
                loadImage(awayTeamLogo, awayTeam?.logo)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recycler_view_main,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = differ.currentList.size

    private fun loadImage(image: ImageView, url: String?) {
        Glide.with(image.context)
            .load(url)
            .placeholder(R.drawable.ic_baseline_image_24)
            .error(R.drawable.ic_baseline_broken_image_24)
            .into(image)
    }
}