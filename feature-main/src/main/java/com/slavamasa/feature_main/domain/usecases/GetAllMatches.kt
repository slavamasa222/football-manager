package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.domain.repository.Repository

class GetAllMatches (
    private val repository: Repository
) {

    suspend operator fun invoke() = repository.getAllMatches()
}