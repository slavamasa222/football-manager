package com.slavamasa.feature_main.domain.usecases

data class SportdataUseCases (
    val getUefaChampionsLeagueMatches: GetUefaChampionsLeagueMatches,
    val getUefaYouthLeagueMatches: GetUefaYouthLeagueMatches,
    val getAllMatches: GetAllMatches,
    val insertMatch: InsertMatch
)