package com.slavamasa.feature_main.domain.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Match(
    @PrimaryKey
    val matchId: Int,

    @Embedded(prefix = "home_")
    val homeTeam: Team? = null,

    @Embedded(prefix = "away_")
    val awayTeam: Team? = null,

    @Embedded(prefix = "stats")
    val stats: Stats? = null,

    val matchStart: String? = null
)