package com.slavamasa.feature_main.domain.repository

import androidx.lifecycle.LiveData
import com.slavamasa.feature_main.data.remote.dto.ResponseDto
import com.slavamasa.feature_main.domain.models.Match

interface Repository {

    suspend fun getUefaYouthLeagueMatches(): ResponseDto

    suspend fun getUefaChampionsLeagueMatches(): ResponseDto

    suspend fun getAllMatches(): List<Match>

    suspend fun insertMatch(match: Match)
}