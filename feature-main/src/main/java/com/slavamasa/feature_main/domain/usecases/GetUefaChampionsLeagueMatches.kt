package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.data.remote.dto.toMatch
import com.slavamasa.feature_main.domain.repository.Repository

class GetUefaChampionsLeagueMatches(
    private val repository: Repository
) {

    suspend operator fun invoke() = repository.getUefaChampionsLeagueMatches().matches.map {it.toMatch()}
}