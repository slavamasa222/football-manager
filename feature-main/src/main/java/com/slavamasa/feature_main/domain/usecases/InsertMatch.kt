package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.domain.models.Match
import com.slavamasa.feature_main.domain.repository.Repository

class InsertMatch (
    private val repository: Repository
) {

    suspend operator fun invoke(match: Match) = repository.insertMatch(match = match)
}