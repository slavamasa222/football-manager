package com.slavamasa.feature_main.domain.models

data class Stats(
    val homeScore: Int,
    val awayScore: Int
)