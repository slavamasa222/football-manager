package com.slavamasa.feature_main.domain.models

import androidx.room.Embedded

data class Team(
    val teamId: Int,
    val name: String? = null,
    val logo: String? = null,

    @Embedded(prefix = "country_")
    val country: Country?
)
