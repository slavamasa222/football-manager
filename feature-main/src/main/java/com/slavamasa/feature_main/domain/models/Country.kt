package com.slavamasa.feature_main.domain.models

data class Country (
    val countryId: Int,
    val name: String? = null
)

